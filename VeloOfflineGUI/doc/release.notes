!-----------------------------------------------------------------------------
! Package     : VeloOfflineGUI
! Responsible : Andrew Crocombe
! Purpose     : Offline GUI for analysis of VELO data
!----------------------------------------------------------------------------

! 2017-06-26 - Anton Poluektov 
 - Changes related to:
     * Fix crash for runs with missing histograms
     * Allow giving a single ROOT file name as --run-data-dir, in which case 
        a single run view is started (with run number 0 and no reference histos)

!====================== VeloOfflineGUI v3r0 2017-01-02 ======================
! 2017-01-02 - Maciej Witold Majewski
 - Lovell and all Lovell projects migrated to gitlab

! 2016-12-01 - Lennaert Bel
 - Add trending to Sensor View tab

! 2016-11-18 - Lennaert Bel
 - Populate Sensor View tab with run plots and IV scans

! 2016-07-21 - Andrew Crocombe
 - Add --iv_data_dir flag to VeloOfflineGUI.exe

!====================== VeloOfflineGUI v2r0 2016-06-21 ======================
! 2016-05-31 - Andrew Crocombe
 - Implement new IV scan method with option for temperature correction 

! 2016-03-30 - Andrew Crocombe
 - Add two dimensional trending plots 

! 2016-02-23 - Andrew Crocombe
 - Add one dimensional trending plots

! 2016-01-05 - Alex Pearce
 - Remove obsolete C++ components, add `lovell` script for running Python GUI

!====================== VeloOfflineGUI v1r3 2015-06-12 ======================
! 2015-06-10 - Alex Pearce
 - Quick fix to support retrieve_run_view_plot.py now returning two plots by
   default

! 2015-04-16 - Alex Pearce
 - Fix image resources not appearing
 - Move QtAssets/ to qt_resources to be consistent with other LHCb Qt
   applications, e.g. Feicim and LHCbDirac.BookkeepingSystem

!====================== VeloOfflineGUI v1r2 2015-03-18 ======================
! 2015-03-18 - Alex Pearce
 - Tweak setting of --run-data-dir option

! 2015-03-11 - Dan Saunders
 - Add --run-data-dir flag to VeloOfflineGUI.exe

! 2015-03-05 - Alex Pearce
 - Tidy up src files

!====================== VeloOfflineGUI v1r1 2015-03-02 ======================
! 2015-02-25 - Marco Clemencic
 - fixed CMakeLists.txt to work with CMake 2.8.9

!====================== VeloOfflineGUI v1r0 2015-02-12 ======================
! 2015-02-12 - J Harrison
  - v1r0 for the first release of the Lovell project
