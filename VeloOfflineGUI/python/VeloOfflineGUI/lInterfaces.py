import sys
from veloview.runview import plots, utils, dq_trends
from veloview.config import Config, run_view, sensor_view
from veloview.special import iv_scans
import velo_view
import special_analyses

def run_list(run_data_dir):
    Config().run_data_dir = run_data_dir
    return utils.run_list()


def trending_variables():
    return dq_trends.get_trending_variables()

def trending_variables_expert():
    return dq_trends.get_trending_variables(expert = True)

def trending_variables_sensor(sensor_id):
    trending_expert = trending_variables_expert()
    return [x for x in trending_expert if "_{0:03d}".format(sensor_id) in x[0]]

def sensor_numbers():
    return dq_trends.get_sensor_numbers()

def iv_files(iv_data_dir):
    Config().iv_data_dir = iv_data_dir
    return iv_scans.iv_datetime_list()


def runview_config():
    return run_view.run_view_pages

def sensorview_config():
    return sensor_view.sensor_view_pages

def veloview_config():
    return velo_view.velo_view_pages


def specialanalyses_config():
    return special_analyses.special_analyses_pages


def runview_plot(run, name, sensor, run_data_dir, refRun = 'Auto', 
                 getRef = False, normalise = False, notifyBox = None):
    Config().run_data_dir = run_data_dir
    err = False
    # Need to append the sensor number to the name.
#     if not utils.valid_run(run):
#         err = True
#         msg = "Invalid run number provided: {0}".format(run)
#         print msg
    if not utils.valid_sensor(sensor):
        err = True
        msg = "Invalid sensor number provided: {0}".format(sensor)
        print msg
        
    name = name.format(sensor)
    
    if getRef:
        if refRun == 'Auto':
            print 'Getting auto ref'
            return plots.get_run_plot_with_reference(name, run, 
                                                     normalise = normalise, 
                                                     notifyBox = notifyBox)
        else:
            print 'Getting specified ref'
            return plots.get_run_plot_with_reference(name, run, refRun=refRun, 
                                                     normalise = normalise, 
                                                     notifyBox = notifyBox)
    else:
        try : 
          return plots.get_run_plot(name, run, normalise = normalise,
                                    notifyBox = notifyBox)
        except KeyError : 
          return None

def veloview_plot(name, runRange, run_data_dir):
    Config().run_data_dir = run_data_dir
    return plots.get_trending_plot(name, runRange) 

    
def veloview_plot2d(nameX, nameY, runRange, run_data_dir):
    Config().run_data_dir = run_data_dir
    return plots.get_2d_trending_plot(nameX, nameY, runRange) 

    
def IV_plot(dataFile, refFile, moduleID, iv_data_dir, getRef = False, notifyBox = None):
    Config().iv_data_dir = iv_data_dir
    nominal, reference, nominal_temp, reference_temp = iv_scans.iv_scan(dataFile, refFile, moduleID)
    if getRef: 
        return nominal, reference, nominal_temp, reference_temp 
    else: return nominal, nominal_temp    


def IV_plot_corr(dataFile, refFile, moduleID, iv_data_dir, getRef = False, notifyBox = None):
    Config().iv_data_dir = iv_data_dir
    nominal, reference = iv_scans.iv_scan_corrected(dataFile, refFile, moduleID)
    if getRef: 
        return nominal, reference
    else: return nominal    


