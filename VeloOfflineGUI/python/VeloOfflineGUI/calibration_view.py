from collections import OrderedDict

PEDESTALS = (
    'pedestal',{
    'plots':
        [
            {
                'title': "pedestal",
                'name': "pedestal",
                'sensor_dependent': False,
                'y_mean_limits': {
                    'overlay': [-60, 60],  # default, or no overlay,
                    'data_minus_ref': [-10, 10],
                    'data_div_ref': [-0.6, 1.4],
                },
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'Pedestal'
    }
)

HIT_THRESHOLD = (
    'hit_threshold',{
    'plots':
        [
            {
                'title': "hit threshold",
                'name': "hit_threshold",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'Hit threshold'
    }
)


LOW_THRESHOLD = (
    'low_threshold',{
    'plots':
        [
            {
                'title': "low threshold",
                'name': "low_threshold",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'low threshold'
    }
)

special_view_pages = OrderedDict([
    PEDESTALS,
    HIT_THRESHOLD,
    LOW_THRESHOLD,
])
