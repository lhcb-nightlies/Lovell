from collections import OrderedDict
from flask import (
    Blueprint,
    current_app,
    request,
    flash,
    redirect,
    url_for,
    render_template,
    abort,
    g
)

from veloview.config import Config as VeloConfig
from veloview.runview import utils as runview_utils
from velo_monitor.utilities import memoize

run_view = Blueprint('run_view', __name__,
                     template_folder='templates/run_view')

# Cache expiry time for checking the latest available run
ONE_HOUR_IN_SECONDS = 1*60*60


@memoize(refresh_rate=ONE_HOUR_IN_SECONDS)
def run_list():
    """Return the list of available runs."""
    return runview_utils.run_list()


def default_run():
    """Retrieve the default run rumber from veloview.

    Used when no run number is specified by the user, or if the given run is
    invalid (as judged by valid_run`).
    """
    return run_list()[0]


def nearby_runs(run, runs, distance=3):
    """Return the runs +/- distance either side of run in runs."""
    idx = runs.index(run)
    lower = idx - distance if idx >= distance else 0
    upper = idx + distance + 1
    return runs[lower:upper]


@run_view.route('/', defaults={'run': None, 'page': '', 'sensor': 0})
@run_view.route('/<int:run>', defaults={'page': '', 'sensor': 0})
@run_view.route('/<int:run>/<page>', defaults={'sensor': 0})
@run_view.route('/<int:run>/<page>/<int:sensor>')
def run_view_builder(run, page, sensor):
    do_redirect = False

    # See if the request was for a particular run/page/sensor and redirect
    # If any GET parameter wasn't specified, or was specified with an invalid
    # value, fall back to the default
    if request.args:
        run = request.args.get('run', run)
        page = request.args.get('page', page)
        sensor = request.args.get('sensor', sensor)
        do_redirect = True

    # Cast int arguments so the `arg in list_of_args` checks work
    invalid_run = False
    invalid_sensor = False
    try:
        run = int(run)
    except (TypeError, ValueError):
        invalid_run = True
    try:
        sensor = int(sensor)
    except (TypeError, ValueError):
        invalid_sensor = True

    # Check if the run number is valid, redirecting to default if not
    runs = run_list()

    if invalid_run or run not in runs:
        new_run = default_run()
        if run is not None:
            flash('Invalid run number {0!r}, reset to {1!r}'.format(
                run, new_run
            ), 'error')
        run = new_run
        do_redirect = True
    # Check if the sensor number is valid, redirecting to default (0) if not
    if invalid_sensor or not runview_utils.valid_sensor(sensor):
        flash('Invalid sensor number {0!r}, reset to 0'.format(sensor),
              'error')
        sensor = 0
        do_redirect = True

    if do_redirect:
        url = url_for('run_view.run_view_builder',
                      run=run,
                      page=page,
                      sensor=sensor)
        return redirect(url)

    # Add the dummy DQ page
    pages = VeloConfig().run_view_pages.items()
    pages.insert(0, ('dq', {'title': 'Data quality'}))
    pages = OrderedDict(pages)

    add_plottable_names(pages)

    # Load the default page from the configuration if we're at the root
    if page == '':
        page = current_app.config['DEFAULT_CHILDREN'].get('run_view', None)
        if page is not None:
            page = page[len('run_view/'):]
    # Else load the page data associated with the route's page
    page_data = pages.get(page, None)

    # Set up the required template variables and render the page
    g.page = page
    g.pages = pages
    g.page_data = page_data
    g.run = run
    g.runs = runs
    g.nearby_runs = nearby_runs(g.run, g.runs)
    g.sensor = sensor
    g.active_page = 'run_view/{0}'.format(page)

    # 404 if the page doesn't exist in the config dict
    if page_data is None:
        abort(404)

    return render_template('run_view/dynamic.html')


# Delegate the page not found hits to the catchall blueprint
@run_view.errorhandler(404)
def page_not_found(e):
    g.pages = VeloConfig().run_view_pages
    g.active_page = 'run_view/404'
    return render_template('run_view/404.html'), 404


# Define a filter used to sanitise the "short name" of run view plots in to
# valid URL hash strings
@run_view.app_template_filter()
def sanitise(s):
    """Return s with all non-alphanumeric characters replaced with '_'."""
    return ''.join([c.lower() if c.isalnum() else '_' for c in s])


def add_plottable_names(pages):
    """Add a `name` key to plots with a `plottables` key.

    Most plots in the run view configuration dictionary correspond to a single
    object in a ROOT file. Some require multiple plots, and these sub-plots are
    called 'plottables'. In order to minimise the differences between how the
    web GUI handles these two cases, we concatenate all plottable names for a
    given plot and use that as the 'plot name'. This name is then split by the
    same token used for concantenation with the front-end requests plot data.

    Options for plottables are appended to the plot's `option` dictionary as
    the `plottables` key, as a list of dictionaries in the same order as the
    plottables.
    """
    separator = current_app.config['PLOTTABLES_SEPARATOR']
    for page in pages.values():
        subpages = page.get('subpages', [page])
        for subpage in subpages:
            for plot in subpage.get('plots', []):
                plottables = plot.get('plottables', [])
                # Always provide a dictionary, even if it's empty
                plot['options'] = plot.get('options', {})
                # Copy the normalisation configuration to the options dict
                # It's easier for us to propagte to the frontend like this
                normalise = plot.get('normalise', False)
                plot['options']['normalise'] = normalise
                if plottables:
                    names, opts = [], []
                    for p in plottables:
                        names.append(p['name'])
                        o = p.get('options', {})
                        # Add the title to the options, so that it can be
                        # displayed in the legend
                        o['title'] = p.get('title', None)
                        opts.append(o)
                    plot['name'] = separator.join(names)
                    plot['options']['plottables'] = opts
