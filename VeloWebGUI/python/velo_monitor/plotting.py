"""Methods to go from data to Bokeh figures."""
from __future__ import division
import copy

from bokeh.colors import RGB
from bokeh.io import gridplot
from bokeh.models import BasicTickFormatter, DataRange1d
from bokeh.models.annotations import Legend, LegendItem
from bokeh.plotting import figure, ColumnDataSource

import numpy as np

from velo_monitor.mpl_compat import hex2color, rgb2hex, viridis

# Default list of colours, applied to plots on the same figure in drawn order
COLOURS = [
    # Blue
    '#4c72b0',
    # Red
    '#c54f52',
    # Green
    '#56a869',
    # Purple
    '#8173b2',
    # Sand
    '#cdba75',
    # Aqua
    '#65b6cd'
]
# Bokeh tools to display
TOOLS = 'pan,box_zoom,reset,save'
# Size of the axis labels in pt
AXIS_FONT_SIZE = 10
# Width of the border line around plots
PLOT_LINE_WIDTH = 1
# Width reserve for colourbar in 2D plots
COLOURBAR_WIDTH = 100


def hex_to_rgb(value):
    """Given a color in hex format, return it in RGB."""
    value = '#' + value if value[0] != '#' else value
    return [255*i for i in hex2color(value)]


def mpl_palette():
    """Return a list of hexademical strings for the viridis colormap."""
    return map(rgb2hex, viridis)


class RGBAColorMapper(object):
    """Maps floating point values to RGB values over a palette"""
    def __init__(self, low, high, palette):
        self.range = np.linspace(low, high, len(palette))
        self.r, self.g, self.b = np.array(zip(*[
            hex_to_rgb(i) for i in palette
        ]))
        # Python 3
        # self.r, self.g, self.b = np.array(list(zip(*[
        #     hex_to_rgb(i) for i in palette
        # ])))

    def color(self, data):
        """Maps your data values to the pallette with linear interpolation"""
        red = np.interp(data, self.range, self.r)
        blue = np.interp(data, self.range, self.b)
        green = np.interp(data, self.range, self.g)
        # Style plot to return a grey color when value is 'nan'
        red[np.isnan(red)] = 240
        blue[np.isnan(blue)] = 240
        green[np.isnan(green)] = 240
        colors = np.dstack([red.astype(np.uint8),
                            green.astype(np.uint8),
                            blue.astype(np.uint8),
                            ])
        return colors[0]


def default_figure(logx=False, logy=False, **kwargs):
    """Return a bokeh Figure with our default options."""
    opts = dict(
        tools=TOOLS,
        toolbar_location='left',
        toolbar_sticky=False,
        logo=None
    )
    opts.update(**kwargs)
    fig = figure(
        x_axis_type='log' if logx else 'linear',
        y_axis_type='log' if logy else 'linear',
        **opts
    )

    fig.xaxis.axis_label_text_font_size = '{0}pt'.format(AXIS_FONT_SIZE)
    fig.yaxis.axis_label_text_font_size = '{0}pt'.format(AXIS_FONT_SIZE)

    fig.outline_line_width = PLOT_LINE_WIDTH
    fig.outline_line_alpha = 1
    fig.outline_line_color = 'black'

    fig.yaxis.formatter = BasicTickFormatter(
        # Use scientific notation from 10^{+/- 3}
        power_limit_low=-3,
        power_limit_high=3
    )

    # Don't allow movement past the extent of the data
    fig.x_range = DataRange1d(bounds='auto')
    fig.y_range = DataRange1d(bounds='auto')

    return fig


def at_least_minimum(val, minimum):
    """Return val if val is greater than or equal to minimum, else minimum."""
    return val if val >= minimum else minimum


def set_padding(fig, top, bottom, left, right):
    if top is not None:
        fig.min_border_top = at_least_minimum(top, PLOT_LINE_WIDTH)
    if bottom is not None:
        bottom = PLOT_LINE_WIDTH
        fig.min_border_bottom = at_least_minimum(top, PLOT_LINE_WIDTH)
    if left is not None:
        fig.min_border_left = at_least_minimum(left, PLOT_LINE_WIDTH)
    if right is not None:
        fig.min_border_right = at_least_minimum(right, PLOT_LINE_WIDTH)


def job_to_figure(data, options, plot_width, plot_height):
    """Return Bokeh figure with the data from an analysis framework job.

    The options argument is the `options` key for the plot from the run view
    configuration, manipulated for plottables by `add_plottable_names`.
    """
    # We don't want to modify the caller's options dict, so copy it
    options = copy.deepcopy(options)
    # List of dictionaries for plottable options
    # The list of empty dicts is for plots with no plottables, so that later
    # the logic is consistent for plots with and without them
    plottable_options = options.pop('plottables', [dict()]*len(data))

    # Need to do some things differently for 2D plots, before doing anything
    is2d = any([d['data']['object_class'].startswith('TH2') for d in data])

    # Smaller main plot to make room for the colourbar
    if is2d:
        plot_width = plot_width - COLOURBAR_WIDTH - 20

    # No plots define a log scale atm, but change this otherwise
    logx = False
    logy = False
    fig = default_figure(
        logx=logx,
        logy=logy,
        width=plot_width,
        height=plot_height
    )
    set_padding(fig, 10, None, 40, 0 if is2d else 20)

    legend_items = []
    for idx, (d, plot_opts) in enumerate(zip(data, plottable_options)):
        data = d['data']['data']
        obj_class = d['data']['object_class']

        # In this way, plottable options override plot options
        opts = options.copy()
        opts.update(plot_opts)

        title = d['data'].get('title', None)
        label = d['data'].get('label', None)
        # Use the plottable title, if present
        if label is None:
            label = opts.get('title', None)
        axis_labels = data['axis_titles']

        fig.title.text = title
        fig.xaxis.axis_label = axis_labels[0]
        fig.yaxis.axis_label = axis_labels[1]

        if obj_class.startswith(('TH1', 'TProfile')):
            # Clamp y-values of first and last bins to zero only for TH1
            clampy = obj_class.startswith('TH1')
            fig, artist = draw_th1(fig, data, legend=label,
                                   color=COLOURS[idx], clampy=clampy)
        elif obj_class.startswith('TH2'):
            fig, artist = draw_th2(fig, data, colourbar_height=plot_height,
                                   as_text=opts.get('asText', False))
            # Can't draw multiple TH2s, so break after the first one
            break
        elif obj_class.startswith('TGraph'):
            fig, artist = draw_tgraph(fig, data, legend=label,
                                      color=COLOURS[idx])
        else:
            print 'Object class {0} not supported'.format(obj_class)
            continue

        if artist is not None and 'mean' in data and 'rms' in data:
            statistics = 'Mean: {0:.2f}; RMS: {1:.2f}'.format(
                data['mean'], data['rms']
            )
            legend_items.append(LegendItem(label=statistics, renderers=[artist]))

    legend = Legend(items=legend_items, location='top_left')
    try:
        fig.add_layout(legend)
        # These settings affect the 'default' legend that diplay plot labels
        fig.legend.position = 'top_right'
        fig.legend.border_line_alpha = 0.5
        fig.legend.background_fill_alpha = 0.5
    except AttributeError:
        # Some figure types, like GridPlot, won't have a legend, so ignore them
        pass

    return fig


def draw_th1(fig, data, legend=None, color=COLOURS[0], logy=False,
             clampy=False):
    """Draw the TH1 represented by data on to the Bokeh figure.

    The histogram is drawn as a stepped line, meaning:
      1. The y value at the lower boundary of each bin is y_1
      2. The first y value at the upper boundary of each bin is y_1
      3. The second y value at the upper bin boundary is y_2
    This creates a line with two segments, horizontal and then vertical

    Keyword arguments:
    fig -- Bokeh figure instance
    data -- Serialised TH1 data
    legend -- Text to use as the entry entry (None to omit entry in legend)
    color -- Colour of TH1 line
    logy -- If the histogram will be drawn on a logarithmic y-scale
    clampy -- Fix the lower edge of the first bin and the upper edge of the
              last bin to have a y-value of zero if True, else they will have
              a y-value as the bin value. If the values are not clamped, the
              y-axis may not display values down to zero.
    """
    # List of 2-element lists of [lower, upper] bin boundaries
    binning = np.array(data['binning'])
    # List of lower boundaries
    lowers = binning[:, 0]
    # List of upper boundaries
    uppers = binning[:, 1]
    # List of bin values
    values = np.array(data['values'])

    nboundaries = binning.size

    xs = np.empty((2 + nboundaries,), dtype=lowers.dtype)
    xs[0] = lowers[0]
    xs[1:-1:2] = lowers
    xs[2:-1:2] = uppers
    xs[-1] = uppers[-1]

    ys = np.empty((2 + nboundaries,), dtype=values.dtype)
    ys[1:-1:2] = values
    ys[2:-1:2] = values

    ymin = values.min()
    # Don't need to fix minimum to zero if the minimum is less than zero
    if clampy and ymin >= 0:
        ys[0] = 0
        ys[-1] = 0
        # Prevent automatic padding
        fig.y_range.start = 0
    else:
        ys[0] = values[0]
        ys[-1] = values[-1]

    # We can't draw y = 0 when the y-axis is logarithmic
    if logy:
        xs = xs[ys > 0]
        ys = ys[ys > 0]

    # Draw x and y coordinates with a linear interpolation
    artist = fig.line(xs, ys, color=color, legend=legend)

    return fig, artist


def draw_th2(fig, data, colourbar_height, as_text):
    """Draw the TH2 represented by data on to the Bokeh figure."""
    # We get a list of [lo, hi] bin boundary pairs...
    xbinning = np.array(data['xbinning'])
    ybinning = np.array(data['ybinning'])
    # ...and a 2D list of bin values, first along x and then along y,
    # i.e. the first entry at [0][0] is for the bin in the lower-left
    # corner and the last at [-1][-1] is for the upper-right
    values = np.array(data['values']).flatten()
    nx = len(xbinning)
    ny = len(ybinning)

    # To draw each rectangle, we need the (x, y) position of its centre
    # and its width and height
    # Each bin centre is just the average of the [lo, hi] boundaries
    # As the values loop over x and then y, the centres in x are
    # repeated, and the centres in y are tiled
    # The bin widths and heights are the difference between the hi and
    # lo edges along x and y, respectively
    xs = np.repeat(np.average(xbinning, axis=1).flatten(), ny)
    ys = np.tile(np.average(ybinning, axis=1).flatten(), nx)
    width = np.repeat(np.diff(xbinning, axis=1).flatten(), ny)
    height = np.tile(np.diff(ybinning, axis=1).flatten(), nx)

    # We need to build our own colour bar
    # To do this, we map a colourscheme, defined as a list of colours,
    # to the domain of our data
    palette = mpl_palette()
    minval, maxval = values.min(), values.max()
    # Force different min/max so that we can display a range of colours
    if minval == maxval:
        minval -= 1
        maxval += 1
    colormap = RGBAColorMapper(minval, maxval, palette)
    # Now we can assign each value a colour
    color = np.array([RGB(*c) for c in colormap.color(values)])

    if as_text:
        # Align text in the center of the cell
        fig.text(
            x=xs[values != 0] - width[values != 0]/2,
            y=ys[values != 0] - height[values != 0]/2,
            text=values[values != 0],
            text_align='center',
            text_baseline='middle'
        )
    else:
        # Use this line to draw all cells
        # fig.rect(x=xs, y=ys, width=width, height=height, color=color)
        # Omit drawing cells with zero count
        fig.rect(
            x=xs[values != 0],
            y=ys[values != 0],
            width=width[values != 0],
            height=height[values != 0],
            color=color[values != 0]
        )

    # Draw bins at the edges so that the original histogram extent is
    # preserved even if the edge bins have a content of zero
    fig.rect(
        x=[xs[0], xs[-1]],
        y=[ys[0], ys[-1]],
        width=[width[0], width[-1]],
        height=[height[0], height[-1]],
        alpha=0
    )

    # Prevent automatic padding for 'normal' histograms, but keep it as as_text
    # ones as the text often extends past the bin boundaries
    if not as_text:
        fig.x_range.range_padding = 0
        fig.y_range.range_padding = 0

    # You can't set Plot.yaxis.location later, see bokeh/bokeh#3016
    # Need to set tools attribute here, as the gridplot call later will only 
    # display tools that are common to all subfigures
    colourbar = default_figure(
        width=COLOURBAR_WIDTH,
        height=colourbar_height,
        x_range=(-0.5, 0.5),
        y_range=(minval, maxval),
        y_axis_location='right',
        toolbar_location=None
    )
    # Set some left padding to be offset from the histogram, but don't set the
    # bottom border, else it won't be auto-adjusted to match that of the
    # histogram figure
    set_padding(colourbar, 10, None, 10, 0)
    colourbar.y_range.range_padding = 0

    axis_labels = data['axis_titles']
    # This label is the z-axis of the 2D histogram
    colourbar.yaxis.axis_label = axis_labels[2]

    # Don't show the x-axis, it's not meaningful
    colourbar.xaxis.visible = False

    # Compute gradient with 100 steps (arbitrary) across the extent of the data
    nsteps = 100
    step_size = (maxval - minval)/nsteps
    # maxval + 1 as we wouldn't reach the very top of the y-axis with maxval
    color_range = np.arange(minval, maxval + 1, step_size)
    colourbar_colors = [RGB(*c) for c in colormap.color(color_range)]
    colourbar.rect(x=0, y=color_range, color=colourbar_colors,
                   width=1, height=step_size)

    # Use gridplot so the subplots share a common toolbar
    # There's a 'feature' that prevents the save button saving the grid, the
    # user will see one modal per subplot
    # See http://stackoverflow.com/a/29210233/596068
    fig = gridplot(
        [[fig, colourbar]],
        border_space=0,
        toolbar_location='left',
        toolbar_options=dict(logo=None)
    )
    return fig, None


def draw_tgraph(fig, data, legend, color):
    """Draw the TGraph represented by data on to the Bokeh figure."""
    source = ColumnDataSource(
        data=dict(
            x=data['x'],
            y=data['y']
        )
    )

    artist = fig.line('x', 'y', source=source, color=color, legend=legend)
    fig.circle('x', 'y', source=source, color=color)

    return fig, artist
