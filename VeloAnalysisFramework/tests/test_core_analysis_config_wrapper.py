from veloview import analysis
from veloview.core.analysis_config_wrapper import AnalysisConfigWrapper
from veloview.core.score_manipulation import ERROR_LEVELS

import tempfile, unittest

class TestAnalysisConfigWrapper(unittest.TestCase):
    def setUp(self):
        self.tempFile = tempfile.NamedTemporaryFile()
        self.tempFile.write('analysis_config_branches = None\nanalysis_config_leaves = None')
        self.tempFile.flush()

        self.wrapper = AnalysisConfigWrapper(self.tempFile.name)
        self.branchesToParse = [
                {"weight": 1.,
                 "children": [],
                 "minWW": 2,
                 "minWE": 8,
                 "minEW": 1,
                 "minEE": 2,
                },
                {"weight": 1.,
                 "minWW": 2,
                 "minEW": 1,
                 "minEE": 2,
                },
                {"children": ["child1", "child2"],
                 "minWW": 1,
                 "minEE": 2,
                }
                ]
        self.branchesRef = [
                {"weight": 1.,
                 "children": [],
                 "thresholds": {ERROR_LEVELS.WARNING: {ERROR_LEVELS.WARNING: 2, ERROR_LEVELS.ERROR: 8},
                                  ERROR_LEVELS.ERROR: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 2, ERROR_LEVELS.FATAL: 1},
                                  ERROR_LEVELS.FATAL: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 1, ERROR_LEVELS.FATAL: 2}},
                },
                {"weight": 1.,
                 "children": [],
                 "thresholds": {ERROR_LEVELS.WARNING: {ERROR_LEVELS.WARNING: 2, ERROR_LEVELS.ERROR: 10},
                                  ERROR_LEVELS.ERROR: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 2, ERROR_LEVELS.FATAL: 1},
                                  ERROR_LEVELS.FATAL: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 1, ERROR_LEVELS.FATAL: 2}},
                },
                {"weight": 1.,
                 "children": ["child1", "child2"],
                 "thresholds": {ERROR_LEVELS.WARNING: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 10},
                                  ERROR_LEVELS.ERROR: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 2, ERROR_LEVELS.FATAL: 1},
                                  ERROR_LEVELS.FATAL: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 1, ERROR_LEVELS.FATAL: 2}},
                },
                ]

        self.leavesToParse = [
                {
                 "title": "Leaf1",
                 "name": "VetraRef1",
                 "errThreshold": 30,
                 "warnThreshold": 60,
                 "comparison": "Chi2Test",
                 "params": "myArg",
                 "weight": .5,
                },
                {
                 "title": "Leaf2",
                 "name": "VetraRef2_{}",
                 "errThreshold": 40,
                 "warnThreshold": 55,
                 "tell1": "R",
                 "comparison": "MeanWidthDiffRef",
                 "params": ".1",
                 "weight": .5,
                },
                {
                 "title": "Leaf3",
                 "name": "VetraRef3_{}",
                 "errThreshold": 0,
                 "warnThreshold": 80,
                 "tell1": "Phi",
                 "comparison": "MeanWidthDiffRef",
                 "params": None,
                 "weight": .8,
                },
                {
                 "title": "Leaf4",
                 "name": "VetraRef4_{}",
                 "errThreshold": 20,
                 "warnThreshold": 10,
                 "tell1": "Both",
                 "comparison": "IntegralThreshold",
                 "params": 10.,
                 "weight": 12.,
                },
                ]

        self.leavesRef = [
                [({"name": "VetraRef1",
                 "path": "VetraRef1",
                 "weight": .5,
                 "maxFatal": 50,
                 "maxError": 30,
                 "maxWarning": 60,
                 },
                 {"comparison": analysis.stats.Chi2Test,
                  "params": "myArg",
                  "trending": [],
                 })],
                self.__toTell1("R", ({
                    "title": "Leaf2",
                    "name": "VetraRef2_{}",
                    "weight": .5,
                    "maxFatal": 50,
                    "maxError": 40,
                    "maxWarning": 55,
                 },
                 {
                    "comparison": analysis.thresholds.MeanWidthDiffRef,
                    "params": ".1",
                 })),
                 self.__toTell1("Phi", ({
                     "title": "Leaf3",
                     "name": "VetraRef3_{}",
                     "weight": .8,
                     "maxFatal": 50,
                     "maxError": 0,
                     "maxWarning": 80,
                 },
                 {
                    "comparison": analysis.thresholds.MeanWidthDiffRef,
                    "params": None,
                 })),
                 self.__toTell1("Both", ({
                     "title": "Leaf4",
                     "name": "VetraRef4_{}",
                     "weight": 12.,
                     "maxFatal": 50,
                     "maxError": 20,
                     "maxWarning": 10,
                 },
                 {
                    "comparison": analysis.thresholds.IntegralThreshold,
                    "params": 10.,
                 })),
                ]


    def tearDown(self):
        self.tempFile.close()
        del self.tempFile

    def testBranch(self):
        for (branchToParse, branchRef) in zip(self.branchesToParse, self.branchesRef):
            self.assertEqual(self.wrapper._parseBranch(branchToParse), branchRef)

        for (leafToParse, leafRef) in zip(self.leavesToParse, self.leavesRef):
            self.assertEqual(self.wrapper._parseLeaf(leafToParse["name"], leafToParse), leafRef)

    def __toTell1(self, rOrPhi, leaf):
        tell1s = []
        if rOrPhi in ["R", "Both"]:
            tell1s += range(0, 42)
        if rOrPhi in ["Phi", "Both"]:
            tell1s += range (64, 64 + 42)

        result = []
        for tell1 in tell1s:
            newLeaf = dict(leaf[0])
            tell1Formatted = "{:03d}".format(tell1)
            newLeaf["title"] += "_" + tell1Formatted
            newLeaf["name"] = newLeaf["name"].format(tell1Formatted)
            newLeaf["weight"] = 1.
            result.append((newLeaf, leaf[1]))

        branch = dict(leaf[0])
        branch["thresholds"] = {ERROR_LEVELS.WARNING: {ERROR_LEVELS.WARNING: 2, ERROR_LEVELS.ERROR: 8},
                                  ERROR_LEVELS.ERROR: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 2, ERROR_LEVELS.FATAL: 1},
                                  ERROR_LEVELS.FATAL: {ERROR_LEVELS.WARNING: 1, ERROR_LEVELS.ERROR: 1, ERROR_LEVELS.FATAL: 2}},
        branch["weight"] = 1.

        branch["children"] = [res[0]["title"] for res in result]
        for key in ["name", "maxError", "maxWarning"]:
            branch.pop(key)
        result.append((branch, {}))
        return result

if __name__ == '__main__':
    unittest.main()
