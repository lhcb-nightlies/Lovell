!-----------------------------------------------------------------------------
! Package     : VeloAnalysisFramework 
! Responsible : Suvayu Ali and Lennaert Bel
! Purpose     : Analysis framework and configuration for offline and web GUIs
!----------------------------------------------------------------------------

! 2017-06-26 - Anton Poluektov 
 - Changes related to:
     * Fix crash for runs with missing histograms
     * Allow giving a single ROOT file name as --run-data-dir, in which case 
        a single run view is started (with run number 0 and no reference histos)

! 2017-05-10 - William Barter
 - Add new 'localfile' boolean flag when running that sets whether full directory structure of run numbers is needed
 
! 2017-03-27 - William Barter
 - change "INSERT" to "INSERT OR IGNORE" in VeloAnalysisFramework/python/veloview/core/io.py

!=================== VeloAnalysisFramework v3r0 2017-01-02 ==================
! 2017-02-06 - Lennaert Bel
 - Finalise analysis configuration 
 - Cleanup analysis functions

! 2017-01-02 - Maciej Witold Majewski
 - Lovell and all Lovell projects are migrated to gitlab. 

! 2016-11-18 - William Barter
 - minor change to VeloAnalysisFramework/scripts/cron_lovell_analyse_runs.bash to reflect use of gcc49 and not gcc48

! 2016-11-18 - Lennaert Bel
 - Add configuration for sensor view

! 2016-11-16 - William Barter
 - Add new monitoring plots for number of r sensor and phi sensor hits associatated with long tracks

!=================== VeloAnalysisFramework v2r0 2016-06-21 ==================
! 2016-06-21 - Jon Harrison
 - Increased timeout for core_combiners test 

! 2016-06-17 - Jon Harrison
 - Removed analysis_toy.qmt and core_analysis_config_wrapper.qmt from tests 

! 2016-06-09 - Kurt Rinnert
 - Add pedestal subtracted ADC summary plot.

! 2016-05-25 - Jon Harrison
 - Removed C++ related commands from CMakeLists

! 2015-12-09 - Marco Gersabeck
 - Removed redundant header cross-talk plots
 - Fixed pedestal plot naming

! 2015-10-23 - Marco Gersabeck
 - Added intermediate merging step to filename logic in veloview/runview/utils.py
 - Used said filename logic directly in plot return method in veloview/runview/plots.py

! 2015-10-23 - Alex Pearce
 - Return z-axis title for 2D plot objects.

! 2015-10-14 - Kurt Rinnert
 - Added normalisation tag to vertex plots; this does not seem to be respected
   by the GUI code yet.

! 2015-08-21 - Alex Pearce
 - Handle plots with no configuration dictionary.
 - Add bad channel plots to run view configuration, on behalf of Sophie
   Richards.

! 2015-08-19 - Michael Alexander
 - Adding IP resolutions plots back. 

! 2015-07-24 - Marco Gersabeck
 - Changed cluster path in python/veloview/config/run_view.py
 - Adding configurable option 'normalised' to scale histograms to unit area

! 2015-07-06 - Alex Pearce
 - Allow any real number type for yAxis{Minimum,Maximum} configuration keys.

! 2015-07-01 - Michael Alexander
 - Removing IP monitoring plots for the moment as they're not made by default.

! 2015-06-24 - Kurt Rinnert
 - Added tracking run view configuration.  For now only for plots produced by
   Vetra w/o postprocessing.  The others would not work yet as they heavily
   rely on overlay and the options dictionary to be functional.

!=================== VeloAnalysisFramework v1r3 2015-06-12 ==================
! 2015-06-12 - Alex Pearce
 - Add bad channels run view configuration, on behalf of Sophie Richards.

! 2015-06-10 - Alex Pearce
 - Improve configuration logic.

   Configuration is now handled by the veloview.config.Config object.
   Any getting or setting of any configuration option should be done with
   this object, e.g. `Config().run_data_dir`.
 - Return (nominal plot, reference plot) to the GUIs by default
 - Add new plot display configuration parameters to the run view config:
   - User-specified y-axis ranges; and
   - Display histograms as points.

! 2015-05-29 - Michael Alexander
 - First commit of config for plots from IP resolution monitoring.

! 2015-05-04 - Manuel Tobias Schiller
 - reduce running time of testtree.py test by reducing the number of dummy
   runs from 64 to 8

! 2015-05-01 - Manuel Tobias Schiller
 - make testtree.py into a proper unit test integrated with QMTest; for now
   it's a very simple standalone python script which signals success/failure
   with the return code of the process

! 2015-04-29 - Manuel Tobias Schiller
 - move testtree.py from src/ to tests/, and modernise it somewhat to cope
   with the new package structure (making it work as a unit test is a second
   step)

!=================== VeloAnalysisFramework v1r2 2015-03-18 ==================
! 2015-03-15 - J Harrison
  - Increase timeout of some QMTests

! 2015-03-05 - A Pearce
  - Fix ROOT dictionary compilation, and make it quieter

! 2015-03-05 - A Pearce
  - More robust temporary file creation in tests

! 2015-03-04 - A Pearce
  - Add run view configuration documentation and associated tests

!=================== VeloAnalysisFramework v1r1 2015-03-02 ==================
! 2015-03-02 - J Harrison / A Pearce
  - Fixes for CMake building and C++ compilation

!=================== VeloAnalysisFramework v1r0 2015-02-12 ==================
! 2015-02-12 - J Harrison
  - v1r0 for the first release of the Lovell project
